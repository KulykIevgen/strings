#ifndef STRINGS_LOCKFREEFORWARDEDLIST_H
#define STRINGS_LOCKFREEFORWARDEDLIST_H

#include <stdexcept>
#include <atomic>

template <typename T>
class ForwardedList
{
    struct link
    {
        T value;
        link* next;
        link(T data):value(data),next(nullptr){ }
    };
    class iterator
    {
    public:
        iterator(link* pointer):Link(pointer){ }
        void operator++()
        {
            if (Link)
            {
                Link = Link->next;
            }
        }
        void operator++(int)
        {
            if (Link)
            {
                Link = Link->next;
            }
        }
        T& operator*()
        {
            if (!Link)
                throw std::logic_error("Unable to dereference nullptr iterator");
            return Link->value;
        }
        bool operator==(const iterator& second)
        {
            return this->Link == second.Link;
        }
        bool operator!=(const iterator& second)
        {
            return !(this->operator==(second));
        }

    private:
        link* Link;
    };
public:
    ForwardedList():first(nullptr),last(nullptr)
    {

    }
    void push_back(T element)
    {
        link* zero = nullptr;
        link* Link = new link(element);
        if (first.compare_exchange_strong(zero,Link))
            return;

        zero = nullptr;
        if (last.compare_exchange_strong(zero,Link))
        {
            auto pointer = first.load();
            pointer->next = last;
            return;
        }

        auto pointer = last.exchange(Link);
        pointer->next = Link;
    }
    T pop_front()
    {
        if (first != nullptr)
        {
            auto temp = first.exchange(first.load()->next);
            auto backup = temp;
            T result = temp->value;
            link* zero = nullptr;
            last.compare_exchange_strong(temp,zero);
            delete backup;
            return result;
        }
        else
            throw std::logic_error("No more elements");
    }
    iterator begin()
    {
        return iterator(first);
    }
    iterator end()
    {
        return iterator(nullptr);
    }
    bool empty()
    {
        return (first == nullptr);
    }
    ~ForwardedList()
    {
        while (!empty())
        {
            pop_front();
        }
    }

private:
    std::atomic<link*> first;
    std::atomic<link*> last;
};

#endif //STRINGS_LOCKFREEFORWARDEDLIST_H
