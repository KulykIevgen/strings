#include "OpenMap.h"
#include <stdexcept>

OpenMapping::OpenMapping(const std::string &name):
        fileopener(name)
{
    mapHandle = CreateFileMappingA(fileopener.GetFileHandle(),nullptr,PAGE_READONLY,0,0, nullptr);
    if (mapHandle == nullptr)
    {
        throw std::logic_error("Unable to map file " + name);
    }
}

OpenMapping::~OpenMapping()
{
    CloseHandle(mapHandle);
}

HANDLE OpenMapping::GetMapHandle() const
{
    return mapHandle;
}