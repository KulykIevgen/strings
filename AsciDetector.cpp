#include "AsciDetector.h"

bool AsciDetector::IsValidSymbol(char symbol) const
{
    return isprint(static_cast<int> (symbol));
}

std::vector<std::string> AsciDetector::ExtractStrings(const std::vector<char> &data) const
{
    std::vector<std::string> result;
    size_t length = 0;

    for (auto it = data.cbegin();it != data.cend();++it)
    {
        if (*it == 0)
        {
            if (length >= 5)
            {
                result.emplace_back(it - length,it - 1);
            }
            length = 0;
        }
        else if (IsValidSymbol(*it))
        {
            ++length;
        }
    }

    return result;
}
