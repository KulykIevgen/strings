#ifndef STRINGS_PROCESSOR_H
#define STRINGS_PROCESSOR_H

#include <string>
#include <atomic>
#include "LockFreeForwardedList.h"

class Processor
{
public:
    explicit Processor(const std::string& StartDir);
    void ManagingWork();

    Processor() = delete;
    Processor(const Processor&) = delete;
    Processor(Processor&&) = delete;
    Processor& operator=(const Processor&) = delete;
    Processor& operator=(Processor&&) = delete;

private:
    ForwardedList<std::string> ansi_strings;
    ForwardedList<std::string> files;
    std::string startDir;
    std::atomic_bool IsWalkingFinished;
    std::atomic_bool IsReadingFinished;

    void DirList();
    void processingFile(const std::string& FilePath);
    void handleFileProcessing();
    void PrintStrings();
};

#endif //STRINGS_PROCESSOR_H
