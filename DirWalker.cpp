#include "DirWalker.h"
#include <windows.h>
#include <iostream>

DirWalker::DirWalker(const std::string &CurrentDir)
{
    directories.push_back(CurrentDir);
}

std::string DirWalker::NextFile()
{
    std::string currentDir;
    std::string result;

    if (!directories.empty())
    {
        currentDir = directories.front();
        directories.pop_front();
    }

    if (!currentDir.empty())
    {
        auto filesInCurrentDir = GetFilesInDir(currentDir);
        files.insert(files.begin(),filesInCurrentDir.begin(),filesInCurrentDir.end());
        auto subDirs = GetSubdirs(currentDir);
        directories.insert(directories.begin(),subDirs.begin(),subDirs.end());
    }

    if (!files.empty())
    {
        result = files.front();
        files.pop_front();
    }

    return result;
}

std::list<std::string> DirWalker::GetSubdirs(const std::string &dir)
{
    return GetElements(dir,false);
}

std::list<std::string> DirWalker::GetFilesInDir(const std::string &dir)
{
    return GetElements(dir,true);
}

std::list<std::string> DirWalker::GetElements(const std::string &dir, bool files)
{
    std::list<std::string> result;
    WIN32_FIND_DATA data = {0};

    std::string Template = dir + "\\*.*";

    HANDLE searchHandle = FindFirstFileA(Template.c_str(),&data);
    if (searchHandle != INVALID_HANDLE_VALUE)
    {
        do{
            std::string ShortName = data.cFileName;
            if (ShortName == "." || ShortName == "..")
                continue;

            auto condition = data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY;
            condition = (files) ? !condition : condition;

            if (condition)
            {
                result.emplace_back(dir + std::string("\\") + data.cFileName);
            }
        }while (FindNextFileA(searchHandle,&data));
        FindClose(searchHandle);
    }

    return result;
}
