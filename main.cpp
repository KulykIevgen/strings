#include "Processor.h"
#include <iostream>

int main(int argc,char* argv[])
{
    if (argc == 2)
    {
        Processor processor(argv[1]);
        processor.ManagingWork();
    }
    else
    {
        std::cout << "Wrong input format\nUse: strings.exe [Full path to scan dir]" << std::endl;
    }
    return 0;
}