#ifndef STRINGS_OPENMAP_H
#define STRINGS_OPENMAP_H

#include "FileOpener.h"
#include <memory>

class OpenMapping
{
public:
    explicit OpenMapping(const std::string& name);
    ~OpenMapping();
    HANDLE GetMapHandle() const;

    OpenMapping() = delete;
    OpenMapping(const OpenMapping&) = default;
    OpenMapping& operator=(const OpenMapping&) = default;
    OpenMapping(OpenMapping&&) = default;
    OpenMapping& operator=(OpenMapping&&) = default;

private:
    FileOpener fileopener;
    HANDLE mapHandle;
};

#endif //STRINGS_OPENMAP_H
