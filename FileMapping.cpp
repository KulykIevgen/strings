#include "FileMapping.h"
#include <stdexcept>

FileMapping::FileMapping(const std::string &name):mapping(name)
{
    mapStart = MapViewOfFile(mapping.GetMapHandle(),FILE_MAP_READ,0,0,0);
    if (mapStart == nullptr)
    {
        throw std::logic_error("Unable to load file mapping for the " + name);
    }
}

FileMapping::~FileMapping()
{
    UnmapViewOfFile(mapStart);
}

size_t FileMapping::GetMapSize() const
{
    MEMORY_BASIC_INFORMATION info = {0};

    if (VirtualQuery(mapStart,&info,sizeof(info)))
    {
        return info.RegionSize;
    }
    return 0;
}

std::vector<char> FileMapping::GetData() const
{
    size_t mappingLength = GetMapSize();
    std::vector<char> result(mappingLength);

    char* begin = reinterpret_cast<char*> (mapStart);
    for (size_t i = 0;i < mappingLength;i++)
    {
        result[i] = begin[i];
    }

    return result;
}
