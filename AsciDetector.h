#ifndef STRINGS_ASCIDETECTOR_H
#define STRINGS_ASCIDETECTOR_H

#include <string>
#include <vector>
#include <cstdint>

class AsciDetector
{
public:
    std::vector<std::string> ExtractStrings(const std::vector<char> &data) const;

    AsciDetector() = default;
    ~AsciDetector() = default;
    AsciDetector(const AsciDetector&) = delete;
    AsciDetector(AsciDetector&&) = delete;
    AsciDetector& operator=(const AsciDetector&) = delete;
    AsciDetector& operator=(AsciDetector&&) = delete;

private:
    bool IsValidSymbol(char symbol) const ;
};

#endif //STRINGS_ASCIDETECTOR_H
