#include <iostream>
#include <thread>

#include "Processor.h"
#include "DirWalker.h"
#include "FileMapping.h"
#include "AsciDetector.h"

Processor::Processor(const std::string &StartDir):IsWalkingFinished(false),
                                                  startDir(StartDir),
                                                  IsReadingFinished(false)
{

}

void Processor::DirList()
{
    DirWalker dirWalker(startDir);
    for (std::string nextFile = dirWalker.NextFile(); !nextFile.empty(); nextFile = dirWalker.NextFile())
    {
        files.push_back(nextFile);
    }
    IsWalkingFinished.store(true);
}

void Processor::processingFile(const std::string &FilePath)
{
    FileMapping fileMapping(FilePath);
    static AsciDetector detector;

    std::vector<char> filedata = fileMapping.GetData();
    const auto strings = detector.ExtractStrings(filedata);
    for (const auto& value:strings)
    {
        std::string resultValue = FilePath + " ==> " + value;
        ansi_strings.push_back(resultValue);
    }
}

void Processor::handleFileProcessing()
{
    while (IsWalkingFinished == false || !files.empty())
    {
        try
        {
            std::string currentFile = files.pop_front();
            processingFile(currentFile);
        }
        catch(const std::exception&){ }
    }
}

void Processor::ManagingWork()
{
    auto coreNumber = std::thread::hardware_concurrency();
    std::vector<std::thread> fileParsers;

    std::thread walker{&Processor::DirList,this};
    std::thread printer{&Processor::PrintStrings,this};
    for (unsigned int i = 0;i < coreNumber - 2;i++)
    {
        fileParsers.emplace_back(&Processor::handleFileProcessing,this);
    }

    walker.join();
    for(auto& parser:fileParsers)
    {
        parser.join();
    }
    IsReadingFinished = true;
    printer.join();
}

void Processor::PrintStrings()
{
    while (IsReadingFinished == false || !ansi_strings.empty())
    {
        try
        {
            std::string currentString = ansi_strings.pop_front();
            std::cout << currentString << std::endl;
        }
        catch(const std::exception&){ }
    }
}
