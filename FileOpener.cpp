#include "FileOpener.h"
#include <stdexcept>

FileOpener::FileOpener(const std::string &name)
{
    fileHandle = CreateFileA(name.c_str(),GENERIC_READ | GENERIC_WRITE,FILE_SHARE_READ |
                                                                       FILE_SHARE_WRITE, nullptr,OPEN_EXISTING,0, nullptr);
    if (fileHandle == INVALID_HANDLE_VALUE)
    {
        throw std::logic_error("Unable to open file " + name);
    }
}

FileOpener::~FileOpener()
{
    CloseHandle(fileHandle);
}

HANDLE FileOpener::GetFileHandle() const
{
    return fileHandle;
}