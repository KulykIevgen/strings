# strings

[![Build status](https://ci.appveyor.com/api/projects/status/4599ir5hewrmhfvy?svg=true)](https://ci.appveyor.com/project/KulykIevgen/strings)

# Note

Simple tool for reverse engineering that can search ansi strings in binaries recursively

# How to use

strings.exe [path to dir with subdirs and files]

**Example**: strings.exe C:\test