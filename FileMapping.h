#ifndef STRINGS_FILEMAPPING_H
#define STRINGS_FILEMAPPING_H

#include "OpenMap.h"
#include <vector>

class FileMapping
{
public:
    explicit FileMapping(const std::string& name);
    ~FileMapping();
    std::vector<char> GetData() const;

    FileMapping() = delete;
    FileMapping(const FileMapping&) = delete;
    FileMapping(FileMapping&&) = delete;
    FileMapping& operator=(const FileMapping&) = delete;
    FileMapping& operator=(FileMapping&&) = delete;

private:
    size_t GetMapSize() const;
    void* mapStart;
    OpenMapping mapping;
};


#endif //STRINGS_FILEMAPPING_H
