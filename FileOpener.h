#ifndef STRINGS_OPENFILE_H
#define STRINGS_OPENFILE_H

#include <string>
#include <windows.h>

class FileOpener
{
public:
    explicit FileOpener(const std::string& name);
    ~FileOpener();
    HANDLE GetFileHandle() const;

    FileOpener() = delete;
    FileOpener(const FileOpener&) = delete;
    FileOpener& operator=(const FileOpener&) = delete;
    FileOpener(FileOpener&&) = default;
    FileOpener& operator=(FileOpener&&) = default;

private:
    HANDLE fileHandle;
};

#endif //STRINGS_OPENFILE_H
