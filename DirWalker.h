#ifndef STRINGS_DIRWALKER_H
#define STRINGS_DIRWALKER_H

#include <string>
#include <list>

class DirWalker
{
public:
    explicit DirWalker(const std::string& CurrentDir);
    std::string NextFile();

    DirWalker() = delete;
    DirWalker(const DirWalker&) = delete;
    DirWalker(DirWalker&&) = delete;
    DirWalker& operator=(const DirWalker&) = delete;
    DirWalker& operator=(DirWalker&&) = delete;

private:
    std::list<std::string> directories;
    std::list<std::string> files;

    std::list<std::string> GetSubdirs(const std::string& dir);
    std::list<std::string> GetFilesInDir(const std::string& dir);
    std::list<std::string> GetElements(const std::string& dir,bool files);
};


#endif //STRINGS_DIRWALKER_H
